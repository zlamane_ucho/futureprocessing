import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import okhttp3.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Communication {

	private static String url;
	private static String login;
	private static String token;

	public Communication(String url, String login, String token){
		Communication.url = url;
		Communication.login = login;
		Communication.token = token;
	}

	public Response makePostRequest(String command, String param, String value) throws IOException {

		OkHttpClient client = new OkHttpClient();

		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, toCommand(command, login, token, param, value).replace("'", "\""));
		Request request = buildRequest(url+"/execute", body);

		return client.newCall(request).execute();
	}

	private static String toCommand(String command, String login, String token, String param, String value) {
		StringBuilder sb = new StringBuilder("{'Command':'");
		sb.append(command).append("',")
				.append("'Login':'").append(login).append("',")
				.append("'Token':'").append(token).append("'");
		if(param == null || param.equals("")) {
			sb.append("}");
			return sb.toString();
		} else {
			sb.append(",'Parameter':'").append(param).append("'");
			if(value == null || value.equals("")){
				sb.append("}");
				return sb.toString();
			} else {
				sb.append(",'Value':'").append(value).append("'")
						.append("}");
				return sb.toString();
			}
		}
	}

	private Request buildRequest(String url, RequestBody body){
		return new Request.Builder()
				.url(url)
				.post(body)
				.addHeader("content-type", "application/json")
				.addHeader("cache-control", "no-cache")
				.build();
	}

	public String getCurrentGameState() throws IOException, JSONException {
		String urlToRead = Communication.url
				+ "/describe?login=" + Communication.login
				+ "&token=" + Communication.token;

		StringBuilder result = new StringBuilder();

		URL urlToSend = new URL(urlToRead);
		HttpURLConnection conn = (HttpURLConnection) urlToSend.openConnection();
		conn.setRequestMethod("GET");
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String line;
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		rd.close();

		System.out.print("Distinct events from game: ");
		System.out.println(makeEventsList(result.toString()));
		return makeJSONReadable(result.toString());
	}

	private String makeJSONReadable(String jsonInLine) throws JSONException {
		JSONObject jsonObject = new JSONObject(jsonInLine);
		jsonObject.remove("events");
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(jsonObject.toString());
		return gson.toJson(je).replace(". ", ".\n\t").replace("! ", "!\n\t");
	}

	private String makeEventsList(String jsonInLine) throws JSONException {
		JSONObject jsonObject = new JSONObject(jsonInLine);

		String events = jsonObject.getString("events");
		events = events.substring(1,events.length()-1);
		List<String> eventsList = Arrays.asList(events.split(","));
		List<String> distinctList = new ArrayList<>();
		for(String event: eventsList){
			if(!distinctList.contains(event)){
				distinctList.add(event);
			}
		}
		return distinctList.toString();
	}
}
