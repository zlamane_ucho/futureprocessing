import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DataReader {
	private List<String> dataList;
	private File file;
	private Scanner scanner;

	public DataReader() {
		this.dataList = new ArrayList<>();
	}

	public DataReader(String path) {
		this.readData(path);
	}

	public List<String> readData(String path){
		this.file = new File(path);
		dataList.clear();
		try {
			this.scanner = new Scanner(this.file);
			while(scanner.hasNext()){
				dataList.add(scanner.nextLine());
			}

		} catch (FileNotFoundException error) {
			error.printStackTrace();
		}
		return this.dataList;
	}

	public List<String> getDataList() {
		return dataList;
	}
}
