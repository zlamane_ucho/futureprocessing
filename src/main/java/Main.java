import org.json.JSONException;

import java.io.IOException;
import java.util.Scanner;

public class Main {

	private static String _url = "https://chaarr.future-processing.pl";
	private static String _login = "tymoteusztelega@gmail.com.google";
	private static String _token = "7F5B2455E5D4AC36F7AF03F3B2411A3B";
	private static String _filePath = "src/main/data.txt";

	private static Communication communication = new Communication(_url, _login, _token);
	private static GameManager gameManager = new GameManager(communication);

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException, JSONException {
		System.out.println("PRESS 1 TO READ WINNING COMMANDS FROM FILE");
		System.out.println("PRESS 2 TO TYPE COMMANDS BY YOURSELF");

		int userChoice = scanner.nextInt();

		switch (userChoice){
			case 1:
				gameManager.readGameCommandsFromFile(_filePath);
				break;
			case 2:
				gameManager.startGameInConsole();
				break;
		}

	}
}