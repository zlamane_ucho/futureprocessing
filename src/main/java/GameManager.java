import org.json.JSONException;

import java.io.IOException;
import java.util.*;

public class GameManager {
	private String _commandsPath = "src/main/commands.txt";
	private String _paramsPath = "src/main/parameters.txt";

	private Map<String, List<String> > commands;
	private List<String> parametersArr;
	private List<String> commandsArr;
	private DataReader dataReader;
	private List<String> commandsFromFile;

	private String command;
	private String param;
	private String value;

	private boolean checkPoludnica;

	private Scanner scanner = new Scanner(System.in);
	private Communication communication;

	public GameManager(Communication communication) {
		this.communication = communication;
		dataReader = new DataReader();
		parametersArr = new ArrayList<>(dataReader.readData(_paramsPath));
		commandsArr = new ArrayList<>(dataReader.readData(_commandsPath));
		commands = new HashMap<>();
		createMapOfCommands();
	}

	public void startGameInConsole() throws JSONException, IOException {
		for ( ; ; ) {
			do {
				System.out.print("Access command: ");
				command = scanner.nextLine();
			} while (!this.checkIfCommandExists(command));
			if (!command.toLowerCase().equals("restart")) {
				do {
					System.out.print("Access parameter: ");
					param = scanner.nextLine();
					if (param.equals("Południca")) {
						checkPoludnica = this.checkIfParameterExists(command, "poludnica");
					}
				} while (!this.checkIfParameterExists(command, param) && !checkPoludnica);
				if (param.toLowerCase().equals("supplies")) {
					do {
						System.out.print("Access value: ");
						value = scanner.nextLine();
					} while (!(Integer.parseInt(value) > 0 && Integer.parseInt(value) < 1001));
				} else if (command.toLowerCase().equals("order")) {
					do {
						System.out.print("Access value: ");
						value = scanner.nextLine();
						if (value.equals("Południca")) {
							checkPoludnica = this.checkIfParameterExists(command, "poludnica");
						}
					} while (!this.checkIfPlaceExists(value) && !checkPoludnica);

				} else {
					value = null;
				}
			} else {
				param = null;
				value = null;
			}
			this.communication.makePostRequest(command, param, value);
			System.out.println("Do you want see log? Y/N");
			String usersChoice = scanner.nextLine();
			if (usersChoice.toLowerCase().equals("y"))System.out.println(this.communication.getCurrentGameState());
		}
	}

	public void readGameCommandsFromFile(String _filePath) throws IOException, JSONException {
		this.commandsFromFile = dataReader.readData(_filePath);
		for (int i = 0; i < this.commandsFromFile.size() / 3; i++) {

			command = commandsFromFile.get(i * 3);
			param = commandsFromFile.get(i * 3 + 1);
			value = commandsFromFile.get(i * 3 + 2);

			if (param.equals("Poludnica")) param = "Południca";
			if (value.equals("Poludnica")) value = "Południca";
			System.out.println("Command: " + command + " Param: " + param + " Value->" + value + "<-");

			okhttp3.Response response = communication.makePostRequest(command, param, value);
			System.out.println(response.code());
		}
		System.out.println(this.communication.getCurrentGameState());
	}

	public boolean checkIfCommandExists(String command){
		String tempString = command.toLowerCase();
		return commands.containsKey(tempString);
	}

	public boolean checkIfParameterExists(String command, String param){
		List<String> temp = commands.get(command.toLowerCase());
		String tempString = param.toLowerCase();
		return temp.contains(tempString);
	}

	public boolean checkIfPlaceExists(String place){
		String temp = place.toLowerCase();
		List<String> tempArr = Arrays.asList(parametersArr.get(0).split(";"));
		return tempArr.contains(temp);
	}

	private void createMapOfCommands(){
		for(int i = 0; i < parametersArr.size(); i++){
			List<String> temp = Arrays.asList(parametersArr.get(i).split(";"));
			this.commands.put(commandsArr.get(i), temp);
		}
	}

	public Map<String, List<String>> getCommands() {
		return commands;
	}
}
